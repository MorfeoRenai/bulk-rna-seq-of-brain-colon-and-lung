# Bulk RNA-seq of Brain Colon and Lung

This repository contains the differential anaysis of bulk RNA-seq data from three tissues: brain, colon and lung.

Final presentation slides can be found [here](./reports/slides.pdf) and edited [here](https://docs.google.com/presentation/d/1WOm0zxTLn4XAcvOj-t_0gIO2C84j7u5NcZwE1Oy4LTY/edit?usp=sharing).


## Data

Data, as count tables and features/samples metadata, available [here](http://159.149.160.56/GT_2022_GTEX/).


## Dependencies

Our project depends on `R=4.3`. To install a specific version of `R` independent of previous installations, plus miscellaneous libraries, we decided to use a `conda` environment.

    $ conda create -f env.yml

In order to use `R=4.3`, remember to activate the `conda` virtual environment.

    $ conda activate bulk-rna-seq-env

In this virtual environment you can run `R` or `rstudio`. They will pick the correct version of R.

Other R dependencies are managed by `renv`. For more info, check out its [web page](https://rstudio.github.io/renv/).

`renv` functionalities are also integrated in [RStudio IDE](https://docs.posit.co/ide/user/ide/guide/environments/r/renv.html) project options. Therefore initializing a RStudio project in the cloned repository is highly recommended.

To install all dependencies, recorded in the `renv.lock` file, run in the R console:

    install.packages("renv")
    renv::restore()


## Pipeline

These are the analysis steps:

1. Quality control of 3 samples from 3 tissues: 9 in total;
2. Filter our rRNA, pseudogenes, mithocondrial, short RNA and non canonical;
3. Quality control plots;
4. Filter by expression level;
5. Normalization with TMM;
6. Multi-Dimensional Scaling;
7. Fit generalized log-linear model with quasi negative binomial distribution;
8. Perform quasi-likelihood F tests (tissue vs tissue);
9. Find differentially expressed genes;
10. Find gene markers;
11. Enrichment analysis;
